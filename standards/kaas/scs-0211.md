# SCS-0211: KaaS Default Storage Class

The SCS-0211 standard outlines the properties required for the default StorageClass in Kubernetes as a Service (KaaS). The standard ensures that the default StorageClass, identified by the "storageclass.kubernetes.io/is-default-class" annotation, supports the ReadWriteOnce access mode and protects volume data against loss due to single disk or host hardware failures.

| Version                                                          | Type     | State     | stabilized | obsoleted |
| ---------------------------------------------------------------- | -------- | --------- | ---------- | --------- |
| [SCS-0211-v1](/standards/scs-0211-v1-kaas-default-storage-class) | Standard | 🟢 Stable | 2023-02-13 | -         |
