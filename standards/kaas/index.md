# KaaS Standards

Standards in this track are concerned with Kubernetes as a Service layer, outlining norms and best practices for deploying, managing, and operating Kubernetes clusters. These standards aim to ensure that the orchestration of containers is streamlined, secure, and compatible across various cloud environments and platforms.

| Standard                             | Most Recent Version                                 | State     | Description           | stabilized |
| ------------------------------------ | --------------------------------------------------- | --------- | --------------------- | ---------- |
| [SCS-0210](/standards/kaas/scs-0210) | [v1](/standards/scs-0210-v1-k8s-new-version-policy) | 🟢 Stable | New Version Policy    | 2023-02-07 |
| [SCS-0211](/standards/kaas/scs-0211) | [v1](/standards/scs-0101-v1-entropy)                | 🟢 Stable | Default Storage Class | 2023-02-13 |
