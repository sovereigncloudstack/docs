# IaaS Standards

The IaaS Layer Standards track focuses on the protocols, guidelines, and specifications that govern the infrastructure as a service layer. This encompasses standards for virtual machines, storage, networking, and other foundational resources, ensuring seamless, efficient, and secure operation, interoperability, and management of the underlying cloud infrastructure.

| Standard                             | Most Recent Version                                         | State     | Description                    | stabilized |
| ------------------------------------ | ----------------------------------------------------------- | --------- | ------------------------------ | ---------- |
| [SCS-0100](/standards/iaas/scs-0100) | [v3](/standards/scs-0100-v3-flavor-naming)                  | 🟢 Stable | Flavor Naming                  | 2023-06-14 |
| [SCS-0101](/standards/iaas/scs-0101) | [v1](/standards/scs-0101-v1-entropy)                        | 🟠 Draft  | Entropy                        | 2023-03-08 |
| [SCS-0102](/standards/iaas/scs-0102) | [v1](/standards/scs-0003-v1-sovereign-cloud-standards-yaml) | 🟢 Stable | Image Meta Data                | 2022-10-31 |
| [SCS-0103](/standards/iaas/scs-0103) | [v1](/standards/scs-0003-v1-sovereign-cloud-standards-yaml) | 🟠 Draft  | Sovereign Cloud Standards YAML | -          |
| [SCS-0104](/standards/iaas/scs-0104) | [v1](/standards/scs-0003-v1-sovereign-cloud-standards-yaml) | 🟠 Draft  | Sovereign Cloud Standards YAML | -          |
