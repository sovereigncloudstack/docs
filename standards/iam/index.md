# IAM Standards

This track revolves around Identity and Access Management (IAM) standards, providing guidelines for ensuring secure and efficient user authentication, authorization, and administration. It addresses issues related to user identity, permissions, roles, and policies, aiming to safeguard and streamline access to cloud resources and services.

| Standard                            | Most Recent Version                                                   | State     | Description                              | stabilized |
| ----------------------------------- | --------------------------------------------------------------------- | --------- | ---------------------------------------- | ---------- |
| [SCS-0300](/standards/iam/scs-0300) | [v1](/standards/scs-0300-v1-requirements-for-sso-identity-federation) | 🟢 Stable | Requirements for SSO identity federation | 2023-06-21 |
