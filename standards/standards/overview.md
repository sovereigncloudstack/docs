# Overview

Standards are the core deliverable of SCS. By standardizing the open source software components of a cloud computing stack, their versions, how they are to be configured, deployed and utilized, SCS guarantees the reproducibility of a certain behavior of this technology.

SCS standards are discussed, developed and maintained in the community by the corresponding teams (see Track in the table below), which naturally include existing users of SCS.

## Stabilized Standards

| Standard                               | Track                       | State     | Description                      | Active Versions           |
| -------------------------------------- | --------------------------- | --------- | -------------------------------- | ------------------------- |
| [SCS-0001](/standards/global/scs-0001) | [Global](/standards/global) | 🟢 Stable | Sovereign Cloud Standards        | v1                        |
| [SCS-0002](/standards/global/scs-0002) | [Global](/standards/global) | 🟢 Stable | Standards, Docs and Organisation | v2                        |
| [SCS-0100](/standards/iaas/scs-0100)   | [IaaS](/standards/iaas)     | 🟢 Stable | Flavor Naming                    | v1 (until 2023-10-31), v3 |
| [SCS-0102](/standards/iaas/scs-0102)   | [IaaS](/standards/iaas)     | 🟢 Stable | Image Metadata                   | v1                        |
| [SCS-0210](/standards/kaas/scs-0210)   | [KaaS](/standards/kaas)     | 🟢 Stable | New Version Policy               | v1                        |
| [SCS-0211](/standards/kaas/scs-0211)   | [KaaS](/standards/kaas)     | 🟢 Stable | Default Storage Class            | v1                        |

## Drafts

| Standard                               | Track                       | State    | Description                              | Active Versions |
| -------------------------------------- | --------------------------- | -------- | ---------------------------------------- | --------------- |
| [SCS-0003](/standards/global/scs-0003) | [Global](/standards/global) | 🟠 Draft | Sovereign Cloud Standards YAML           | -               |
| [SCS-0101](/standards/iaas/scs-0101)   | [IaaS](/standards/iaas)     | 🟠 Draft | Entropy                                  | -               |
| [SCS-0103](/standards/iaas/scs-0103)   | [IaaS](/standards/iaas)     | 🟠 Draft | Standard Flavors                         | -               |
| [SCS-0104](/standards/iaas/scs-0104)   | [IaaS](/standards/iaas)     | 🟠 Draft | Standard Images                          | -               |
| [SCS-0300](/standards/iam/scs-0300)    | [IAM](/standards/iam)       | 🟠 Draft | Requirements for SSO identity federation | -               |
| [SCS-0412](/standards/ops/scs-0412)    | [Ops](/standards/ops)       | 🟠 Draft | Exposition of IaaS metering data as JSON | -               |
