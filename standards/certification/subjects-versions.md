# Scopes and Versions

The SCS Certification scopes are discriminated in two dimensions. The first is the technology layer, which is either IaaS (infrastructure) or KaaS (Kubernetes). The second dimension is the level, which refer to the level of sovereignty, which is achieved by the certificate. There are three different levels:

- SCS compatible
- SCS open
- SCS sovereign

Consequently, an SCS user can achieve certificates in six different scopes.
Hence, an SCS certification scope examines all standards required to achieve a certain level for a certain layer, e.g. the "SCS-compatible" level for the IaaS-layer.

Like software states, certification scopes are versioned. To achieve the certificate for a scope all standards, which are part of the current version of this scope must be met. A certification scope version has an expiry date after which the next version of this scope becomes valid. An SCS user must fulfil the standards in the actual certification scope version to stay certified. There is a transition period between two versions, in which both versions are valid as shown in the diagram below. It also shows that each scope has its own versioning cycle independent from all other scopes.

![Alt text](image.png)
