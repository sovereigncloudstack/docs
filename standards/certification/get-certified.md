# How to get certified

1. (optional) Download the compliance check tool and run it on your environment in order to check that all standards for the certification scope are met.
2. Contact the SCS project team and request to officially certified.
3. Prepare yourself to give the SCS project team user access to your cloud environment
4. The SCS project team will run regular tests (the same as the compliance check tool), in order to check whether all standards compiled in the certification scope are met.
5. If so, then your environment is listed in the "certified clouds" section on this site.

The above workflow applies at the moment to the certification scope SCS-compatible on IaaS. For more complex standards to be met, which may not be automatically and remotely tested.
For cloud environments, which do not provide public access for various reasons, but do want to accomplish a certification an individual process will provided accordingly.
