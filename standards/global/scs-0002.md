# SCS-0002: Org & Docs Standards

SCS-0002 standard outlines the standardized structure and maintenance processes for easily accessible and comprehensible content of the SCS project.

| Version                                                  | Type       | State     | stabilized | obsoleted |
| -------------------------------------------------------- | ---------- | --------- | ---------- | --------- |
| [SCS-0002-v1](/standards/scs-0002-v1-standards-docs-org) | Procedural | 🟢 Stable | 2023-02-06 | -         |
| [SCS-0002-v2](/standards/scs-0002-v2-standards-docs-org) | Procedural | 🟢 Stable | 2023-08-03 | -         |
