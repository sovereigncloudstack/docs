# Global Standards

This track encompasses the foundational standards that guide the overall structure, documentation, and general topics related to the Sovereign Cloud Stack. It serves as the core framework, ensuring consistency, clarity, and comprehensibility across all aspects of the cloud stack, fostering an environment where information is easily accessible and understood.

| Standard                              | Most Recent Version                                         | State     | Description                      | stabilized |
| ------------------------------------- | ----------------------------------------------------------- | --------- | -------------------------------- | ---------- |
| [SCS-001](/standards/global/scs-0001) | [v1](/standards/scs-0001-v1-sovereign-cloud-standards)      | 🟢 Stable | Sovereign Cloud Standards        | 2022-11-28 |
| [SCS-002](/standards/global/scs-0002) | [v2](/standards/scs-0002-v2-standards-docs-org)             | 🟢 Stable | Standards, Docs and Organisation | 2023-03-08 |
| [SCS-003](/standards/global/scs-0003) | [v1](/standards/scs-0003-v1-sovereign-cloud-standards-yaml) | 🟠 Draft  | Sovereign Cloud Standards YAML   | -          |
